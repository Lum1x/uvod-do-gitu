Diktáty z https://www.umimecesky.cz/diktaty

| Kód studenta | Vypracovaná úloha    | Code review |
| ------------ | -------------------- | ----------- |
| st12345      | 00_demo_zadani.txt   | st54321     |
| st12125      | -                    | -           |


### Požadavky k získání zápočtu

1. Vytvořit issue, pomocí kterého si zarezervujete konkrétní zadání
2. Vytvořit fork projektu
3. Fork lokálně naklonovat
4. Lokáně vytvořit větev dle čísla a názvu přiřazeného issue
5. Zapracovat změny (s tím, že úmyslně vytvoříte 3-4 chyby)
6. Pushnut změny do svého forku
7. Vytvořit MR (merge request) ze své větvé do větve `master` v `upstream` repozitáři
8. Vyberete si jedno otevřené MR (svého kolegy) a uděláte code review
9. Počkáte, než vám někdo udělá CR a následně chyby z něj opravíte a pushnete (do svého forku)
